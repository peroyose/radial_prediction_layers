import argparse
import os
import sys
import shutil
from urllib.request import urlretrieve

parser = argparse.ArgumentParser("RPL Download Pretrained Models")

parser.add_argument('--data', type=str, choices=["ALL", "MNIST", "CIFAR10", "CIFAR100", "IMAGENET"], help="Download Pretrained Models for a specific experiment set")
parser.add_argument('--force', '-f', action='store_false', help="Force download even if you already have local models")
parser.add_argument('--removelocal', '-rml', action='store_true', help="Remove local models before downloading the pretrained once")

config = {"cifar10": "https://redmine.f4.htw-berlin.de/owncloud/index.php/s/JCMfqSzpCQE9n8r/download",
          "cifar100": "https://redmine.f4.htw-berlin.de/owncloud/index.php/s/CF8eLkmL88DGxnH/download",
          "mnist": "https://redmine.f4.htw-berlin.de/owncloud/index.php/s/64SL3axz4QmdqeF/download",
          "imagenet": "https://redmine.f4.htw-berlin.de/owncloud/index.php/s/rynkw7PCgtpJt3P/download"}


def create_dir(path):
    """ Create dir if it not exist
    """
    if not os.path.exists(path):
        os.makedirs(path)
        print("Created dir: {}".format(path))
    return None


def hook_progress(num_block, size_block, size_total):
    """ Print progress bar during download
    """
    readsofar = num_block * size_block
    if size_total > 0:
        percent = readsofar * 1e2 / size_total
        s = "\r%5.1f%% %*d / %d" % (percent, len(str(size_total)), readsofar, size_total)
        sys.stderr.write(s)
        if readsofar >= size_total:
            sys.stderr.write("\n")
    else:
        sys.stderr.write("read %d\n" % (readsofar,))


if __name__ == "__main__":
    args = parser.parse_args()

    if args.data == "ALL":
        download_list = config
    else:
        download_list = {data: url for data, url in config.items() if data == args.data.lower()}

    for data, url in download_list.items():
        file_dir = os.path.join("./models/", data)
        file_path = os.path.join(file_dir, data + "tar.gz")

        # Create directory if it not exist
        create_dir(file_dir)

        # If 'removelocal' flag is set remove all .pt files from the directory
        if args.removelocal:
            for model in os.listdir(file_dir):
                if ".pt" in model:
                    os.remove(model)

        # If 'force' flag is NOT set abort if directory contains any .pt files
        if args.force:
            if any(file.endswith('.pt') for file in os.listdir(file_dir)):
                print("There are already models (*.pt files) in {}. Run the script with".format(file_dir))
                print("-f to download anyway (possible name conflicts)")
                print("-rml to remove local models before download")
                sys.exit()

        # Download model achive
        urlretrieve(url, file_path, hook_progress)

        # Unzip
        shutil.unpack_archive(file_path, extract_dir=file_dir, format="gztar")

        # Remove downloaded achive
        os.remove(file_path)

        print("Pretrained models for '{}' experiments downloaded and unpacked".format(data))
