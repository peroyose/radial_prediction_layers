import os

import glob
import torch
from tqdm import tqdm

from utils.utils import accuracy
from utils.networks import LeNet, AlexNet, VGG19, ResNet50
from utils.uncertainty import ModelWithTemperature, DeepEnsemble


def init_correct_model(method, network, num_classes):
    if "SOFT" in method:
        if network == "LeNet":
            return LeNet(num_classes=num_classes)
        if network == "AlexNet":
            return AlexNet(num_classes=num_classes)
        if network == "VGG":
            return VGG19(num_classes=num_classes, use_rpl=False)
        if network == "ResNet":
            return ResNet50(num_classes=num_classes)
    if "RPL" in method:
        if network == "LeNet":
            return LeNet(num_classes=num_classes, use_rpl=True)
        if network == "AlexNet":
            return AlexNet(num_classes=num_classes, use_rpl=True)
        if network == "VGG":
            return VGG19(num_classes=num_classes, use_rpl=True)
        if network == "ResNet":
            return ResNet50(num_classes=num_classes, use_rpl=True)
    else:
        print("WTF")


def load_pretrained_models(path, num_classes=10, device=None, verbose=False):
    """
    """

    if device is None:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    model_list = (glob.glob(path + "*"))
    model_list = [file.replace(path, "") for file in model_list]
    model_list = sorted(model_list)
    print("Found following models in {}: \n{}".format(path, model_list))

    # load the model into a list
    loaded_models = []
    for model in model_list:
        model_info = model.split("-")
        print(model_info[0], model_info[1])
        init_model = init_correct_model(model_info[0], model_info[1], num_classes)
        if any("temp" in sub_str for sub_str in model_info):
            init_model = ModelWithTemperature(init_model)
        if any("ENSEMBLE" in sub_str for sub_str in model_info):
            init_ensemble = DeepEnsemble(init_model)
            init_ensemble.load(os.path.join(path, model))
            loaded_models.append(init_ensemble)
        else:
            init_model.load_state_dict(torch.load(path + model, map_location=device))
            loaded_models.append(init_model)
        if verbose:
            print(init_model)

    return loaded_models


def create_plot_names_from_path(path, beta_list=None):
    labels = []

    model_list = (glob.glob(path + "*[!.npz]"))
    model_list = [file.replace(path, "") for file in model_list]
    if "train.log" in model_list:
        model_list.remove("train.log")
    model_list = sorted(model_list)

    for model in model_list:
        model = model.replace(".pt", "")
        model_split = model.split("-")
        model_split = [word for word in model_split if not word.startswith("acc")]
        if beta_list is not None and "RPL" in model_split[0]:
            for beta in beta_list:
                beta_str = "b" + str(beta).replace(".", "p")
                labels.append("-".join(model_split) + "-" + beta_str)
        else:
            labels.append("-".join(model_split))

    return labels


def mult_prediction(model_path, data_loader, num_classes=10, beta_list=None, convert_np=True, device=None):
    """
    """
    if device is None:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    # Check path for state_dicts and ensemble folders
    model_list = (glob.glob(model_path + "*[!.npz]"))
    model_list = [file.replace(model_path, "") for file in model_list]
    if "train.log" in model_list:
        model_list.remove("train.log")
    model_list = sorted(model_list)
    print("Found following models in {}: \n{}".format(model_path, model_list))

    # Define return vars
    output_models = []
    output_targets = []
    accuracies = []

    for i, model in enumerate(model_list):
        print("Predict model {}".format(model))
        # Initialize model
        model_info = model.split("-")
        init_model = init_correct_model(model_info[0], model_info[1], num_classes)
        if any("temp" in sub_str for sub_str in model_info):
            init_model = ModelWithTemperature(init_model)

        # Load model
        if any("ENSEMBLE" in sub_str for sub_str in model_info):
            init_ensemble = DeepEnsemble(init_model)
            init_ensemble.load(os.path.join(model_path, model))
            loaded_model = init_ensemble
        else:
            init_model.load_state_dict(torch.load(model_path + model, map_location=device))
            loaded_model = init_model

        # Ensembles got their own prediction method
        if isinstance(loaded_model, DeepEnsemble):
            if hasattr(loaded_model.model, 'use_rpl') and loaded_model.model.use_rpl is True and beta_list is not None:
                beta_values = beta_list
            else:
                beta_values = [1.0]

            for beta in beta_values:
                output_ens, _, target_ens = loaded_model.predict(data_loader, beta)
                acc_ens = (((target_ens == output_ens.argmax(dim=1)).sum()).item() / len(data_loader.dataset)) * 100
                accuracies.append(acc_ens)
                if convert_np:
                    output_models.append(output_ens.cpu().detach().numpy())
                    output_targets.append(target_ens.cpu().detach().numpy())
                else:
                    output_models.append(output_ens)
                    output_targets.append(target_ens)

        # Predict all other models than ENSEMBLES
        else:
            # Put all models to device and into eval() mode
            loaded_model.to(device)
            loaded_model.eval()

            if hasattr(loaded_model, 'use_rpl') and loaded_model.use_rpl is True and beta_list is not None:
                beta_values = beta_list
            else:
                beta_values = [1.0]

            for beta in beta_values:

                batch_output = []
                batch_targets = []
                batch_acc = []

                # Predict set
                with torch.no_grad():
                    for data, target in tqdm(data_loader):
                        # Sends data to GPU
                        data = data.to(device)
                        target = target.to(device)

                        # Forward batch to get logits
                        logits = loaded_model.forward(data)
                        props = torch.exp(beta * logits)

                        # Add batch outputs to the model outputs
                        batch_output.append(props)
                        batch_targets.append(target)
                        batch_acc.append(accuracy(props, target)[0])

                accuracies.append((torch.sum(torch.cat(batch_acc)) / len(batch_acc)).item())

                if convert_np:
                    output_models.append(torch.cat(batch_output).cpu().detach().numpy())
                    output_targets.append(torch.cat(batch_targets).cpu().detach().numpy())
                else:
                    output_models.append(torch.cat(batch_output))
                    output_targets.append(torch.cat(batch_targets))

        # Clean up GPU memory after detach
        del loaded_model
        torch.cuda.empty_cache()

    return accuracies, output_models, output_targets
