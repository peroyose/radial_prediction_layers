import os
import csv
import urllib

import numpy as np
import scipy.ndimage as nd

from tqdm import tqdm
import torch

import matplotlib.pyplot as plt
import torchvision.utils as vutils

torch.manual_seed(20)
np.random.seed(20)


class AverageMeter():
    """ Container for logging and printing stats in a training process

    Following function is based on https://github.com/pytorch/examples/blob/master/imagenet/main.py
    Copyright (c) 2016, Facebook, Inc. All rights reserved.

    Args:
        name: Name used if printing the value
        fmt: Format of the string
    """

    def __init__(self, name, fmt=":.2f"):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0
        self.hist = []

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count
        self.hist.append(val)

    def __str__(self):
        fmtstr = "{name}: {val" + self.fmt + "} (⌀:{avg" + self.fmt + "})"
        return fmtstr.format(**self.__dict__)


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k

    Following function is based on https://github.com/pytorch/examples/blob/master/imagenet/main.py
    Copyright (c) 2016, Facebook, Inc. All rights reserved.

    """
    with torch.no_grad():
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0, keepdim=True)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


def print_learnable_params(model, freezed_too=False, log_file=None):
    """ Print (learnable) parameters in a given network model

    Args:
        model: Model you want to print the learned parameters
        freezed_too: Print the freezed parameters as well
        log_file: if a opened log file is provided, information is written there]
    """
    updated = []
    freezed = []

    for name, param in model.named_parameters():
        if param.requires_grad:
            updated.append(name)
        else:
            freezed.append(name)

    print("\nFollowing parameters of the model will be updated:", file=log_file)

    for para in updated:
        print("- {}".format(para), file=log_file)
    if freezed_too is True:
        print("\n Following parameters of the model are freezed:", file=log_file)
        for para in freezed:
            print("- {}".format(para), file=log_file)


def freeze_pretrained_params(model):
    """ Iterates over learnable parameter and set required_grad = False
    """
    for param in model.parameters():
        param.requires_grad = False


def predict_probabilities(model, dataloader, beta=1.0, device=torch.device("cpu")):
    """ Predicts data for a given model and a certain beta value (RPL networks)

    Args:
        model: A RPL model
        dataloader: PyTorch dataloader
        beta: Shifts the prediction of an RPL network
        device: Torch.device to run the calculation
    """
    wrong_probs = []
    correct_probs = []

    for data, target in dataloader:
        # Sends data to GPU
        data = data.to(device)
        target = target.to(device)

        # Predict batch
        output = model.forward(data)
        output = torch.exp(beta * output)

        # Get max values and indicies
        preds = torch.max(output, 1)

        # Store probabilties in two lists
        for i, pred in enumerate(preds[1]):
            prob_i = preds[0][i].detach().cpu().numpy()

            # get all probs from top1 pred
            if pred.item() != target[i].item():
                wrong_probs.append(prob_i)
            else:
                correct_probs.append(prob_i)

    return correct_probs, wrong_probs


def get_rotated_images(dataloader, image_dim, max_angle=180, rotation_number=None, force_class=None, device=None, verbose=False, save_fig=None):
    """ Creates a batch of rotated images of a given base image

    Args:
    dataloader: torch.utils.data.dataloader
        A dataloader with images (only one channel)
    image_dim: (Int, Int)
        Tupel of image dimensions, e.g. MNIST -> (28,28)
    max_degree: Int - default: 180
        Maximum roation angle
    rotation_number: Int - default: None
        Number of equally distributed angles in the interval [0,max_degree].
        If 'None' image will be rotated every 10 degrees.
    force_class:
        Force the sample to be of a speficic class
    verbose: Bool - default: False
        Plots a grid of the rotated batch
    save_fig: String - default: None
        If a path is given, the plot will be saved in that location.

    Returns:
    rotation_batch: pytorch.tensor
        Tensor with rotated images
    rotation_targets: pytorch.tensor
        Tensor with corresponding targets for the rotated image
    rotations: list
        List of angles of the rotated images
    """
    # Get a batch
    if device is None:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    batch, targets = next(iter(dataloader))

    # Get an image from the batch
    if force_class is None:
        image = batch[np.random.randint(0, dataloader.batch_size + 1)]
    else:
        assert force_class in targets, "force_class not found in targets"
        # TODO: Do not take the first class example, sample one!
        class_index = np.where(targets == force_class)[0]
        image = batch[class_index[0]]

    # Create list of rotations depending on max_angle and rotation_number
    if rotation_number is None:
        rotation_number = max_angle // 10 + 1
    angles = np.linspace(0, max_angle, rotation_number)

    # Placeholder tensor for the rotated batch
    rot_images = np.zeros((rotation_number, image_dim[0], image_dim[1]))

    # For each angle rotate the base image and add to the placeholder in the corresponding position
    for i, degree in enumerate(angles):
        rot_img = nd.rotate(image.reshape(image_dim), degree, reshape=False)
        rot_img = np.clip(a=rot_img, a_min=0, a_max=1)  # Clip image so its value range is still [0,1]
        rot_images[i] = rot_img

    # Create a PyTorch Tensor from the numpy tensor and add the channel dimension
    rotation_batch = torch.from_numpy(rot_images).view(rotation_number, 1, image_dim[0], image_dim[1])
    rotation_batch = rotation_batch.type(torch.float32)
    # Create a target tensor
    rotation_targets = torch.Tensor([targets[0].item()] * rotation_number)

    # Plot the rotated image
    if verbose:
        plt.figure(figsize=(20, 10))
        plt.axis("off")
        plt.imshow(np.transpose(vutils.make_grid(rotation_batch, nrow=rotation_number, padding=2, normalize=True).cpu(), (1, 2, 0)))
        if save_fig is not None:
            plt.savefig(save_fig, bbox_inches="tight")
        plt.show()

    rotation_targets.to(device)
    rotation_batch.to(device)

    data_set = torch.utils.data.TensorDataset(rotation_batch, rotation_targets)
    data_loader = torch.utils.data.DataLoader(data_set)

    return data_loader, angles


def save_to_csv(result_dict, csv_filename):
    with open(csv_filename, 'a', newline='') as csvfile:

        fileEmpty = os.stat(csv_filename).st_size == 0

        fieldnames = list(result_dict.keys())
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        if fileEmpty:
            writer.writeheader()
        writer.writerow(result_dict)


# https://stackoverflow.com/questions/15644964/python-progress-bar-and-downloads
class DownloadProgressBar(tqdm):
    def update_to(self, b=1, bsize=1, tsize=None):
        if tsize is not None:
            self.total = tsize
        self.update(b * bsize - self.n)


def download_url(url, output_path):
    with DownloadProgressBar(unit="B", unit_scale=True, miniters=1, desc=url.split("/")[-1]) as t:
        urllib.request.urlretrieve(url, filename=output_path, reporthook=t.update_to)
