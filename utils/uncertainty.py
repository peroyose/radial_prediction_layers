import time
import os
import glob
import copy
from tqdm import tqdm
import logging
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from utils.plots import *


class DeepEnsemble():
    """ Implements DeepEnsembles for the classification problem.

    Implementation of method DeepEnsembles presented in the publication:
    https://arxiv.org/pdf/1612.01474.pdf. The implementation is only
    for the classification not the regression problem.

    Args:
        model (torch.nn.Module): Template model for the ensemble
        ensemble_size (int): Number of instances created or loaded of the template model in the ensemble (default: 10)
        device (torch.device): Device where the ensemble is trained on. (default: None)
    """

    def __init__(self, model, ensemble_size=5, device=None):
        self.model = model
        self.ens_size = ensemble_size
        self.ens_models = []
        self.ens_acc = []

        # Use CUDA is available
        if device is None:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device

    def train(self, data, optimizer, criterion, epochs=10, lr=0.001, adverserial_epsi=0., ensemble_path=None, verbose=False):
        """ Trains an ensemble on a given data set with given training conditions.

        Args:
            data (torch.DataLoader): Data models in the ensemble are trained on
            optimizer (torch.optim): Optimizer used for each model training
            criterion (torch.nn): Function used as loss
            epochs (int): Number of epochs traning each model
            adverserial_epsi (float): If epsi is > 0. trains addtionally adverserial using the 'fast
                                      gradient sign method' with 'adverserial_epsi' representing the
                                      epsilon values used in the method.
            verbose (Bool): If set prints additional informations during training
        """
        self.epochs = epochs
        self.adverserial_epsi = adverserial_epsi
        self.criterion = criterion
        self.verbose = verbose

        logging.info("Starting training of an DeepEnsemble with {} instances:".format(self.ens_size))
        for i in range(self.ens_size):
            # Initialize model instance
            model_instance = copy.deepcopy(self.model)
            model_instance.train()
            model_instance.to(self.device)

            # Initialize optimizer and loss function
            optimizer_instance = optimizer(model_instance.parameters(), lr=lr)

            # Train model
            logging.info("Start training model [{}/{}]".format(i + 1, self.ens_size))
            model_instance, acc_instance = self._train_instance(model_instance, data, optimizer_instance, adverserial_epsi, verbose)

            # If on 'ensemble_path' is given models will be saved and only the path stored in 'ens_models'
            if ensemble_path is not None:
                if not os.path.isdir(ensemble_path):
                    os.makedirs(ensemble_path)
                    logging.info("Created directory {}".format(ensemble_path))

                file_name = "ENSEMBLE-model_{}.pt".format(i)
                file_path = os.path.join(ensemble_path, file_name)

                # If we calculated on multiple GPUs save model without DataParallel
                if isinstance(model_instance, torch.nn.DataParallel):
                    torch.save(model_instance.module.state_dict(), file_path)
                else:
                    torch.save(model_instance.state_dict(), file_path)
                self.ens_models.append(file_path)
            else:
                self.ens_models.append(model_instance)

            self.ens_acc.append(acc_instance)

            # Clean up and empty buffer
            del model_instance, optimizer_instance
            torch.cuda.empty_cache()

        logging.info("Finished training of {} models, following accuracy where achived:".format(i))
        for i, acc in enumerate(self.ens_acc):
            logging.info("Model {} --> {}".format(i, acc))

    def _train_instance(self, model, data, optimizer, fgsm_epsi, verbose):
        """ Main trainings loop for one model of the ensemble.

        Runs the traning of a model for the defined epoch number in the ensemble.
        If set adverserial traning (fast gradient sign method) is used as well.

        Args:
            model (nn.Module): Instance of the ensemble model
            optimizer (torch.optim): Instance of the optimizer used for the model instance
            fgsm_epsi (float): Epsilon in the FGSM(ethode)
            verbose (Bool): If set print additionla training information

        Retruns:
            torch.nn.Module: Trained model instance
            float: Accuracy of the model on the training data
        """
        # Time logger
        since = time.time()

        for e in range(self.epochs):
            # Initialize log vars
            loss_sum = 0
            correct_preds = 0
            accuracy = 0

            for images, targets in data:
                # Move batch to device
                x = images.to(self.device)
                y = targets.to(self.device)

                # Clean buffer
                optimizer.zero_grad()

                # Calculate output and loss
                output = model.forward(x)
                loss = self.criterion(output, y)

                # If set use adverserial traning
                if self.adverserial_epsi > 0.:
                    loss.backward(retain_graph=True)
                    perturbed_x = self._fgsm(x, loss)
                    perturbed_output = model.forward(perturbed_x)
                    perturbed_loss = self.criterion(perturbed_output, y)
                    perturbed_loss.backward()
                else:
                    loss.backward()

                optimizer.step()

                # Accumulate batch loss and number of correct classified samples
                if self.adverserial_epsi is True:
                    loss_sum += loss + perturbed_loss
                else:
                    loss_sum += loss
                correct_preds += (y == output.argmax(dim=1)).sum()

            epoch_loss = loss_sum / len(data)
            accuracy = correct_preds.item() / len(data.dataset)
            if self.verbose:
                logging.info("E[{}] || Loss: {:4f} || Correct Classified: {} || Accuracy: {:4f}".format(e, epoch_loss, correct_preds, accuracy))

        time_elapsed = time.time() - since
        logging.info("Training of model finished in {:.0f}m {:.0f}s".format(time_elapsed // 60, time_elapsed % 60))

        return model, accuracy

    def predict(self, dataset, beta=1.0, prediction_size=0):
        """ Predicts a given dataset using all models in the ensemble.

        Args:
            dataset (torch.DataLoader): PyTorch DataLoader for a dataset

        Returns:
            torch.Tensor: Probability of the classes averaged over the ensenble models
            torch.Tensor: List of probabilities of the classes for each model in the ensemble
            torch.Tensor: List of targets in the order used in the prediction process
        """
        if prediction_size == 0 or prediction_size > self.ens_size:
            prediction_size = self.ens_size

        output_ensemble = []
        output_targets = []
        correct_preds = []

        # Ensure all models are in eval mode and create placeholder
        for i, model in enumerate(self.ens_models):
            if i == prediction_size:
                break
            model.eval()
            output_ensemble.append([])
            correct_preds.append(0)

        with torch.no_grad():
            for data, target in tqdm(dataset):
                # Sends data to GPU
                data = data.to(self.device)
                target = target.to(self.device)

                # Predict batch
                output_targets.append(target)
                for i, model in enumerate(self.ens_models):
                    if i == prediction_size:
                        break
                    logits = model.forward(data)
                    props = torch.exp(beta * logits)
                    output_ensemble[i].append(props)
                    correct_preds[i] += (target == props.argmax(dim=1)).sum()

            # for i, model in enumerate(self.ens_models):
            #     if i == prediction_size:
            #         break
            #     print("Model Accuracies: ", correct_preds[i].item() / len(dataset.dataset))

        # Concat list of tensors and stack them to one tensor
        output_targets = torch.cat(output_targets)
        for i, prediction in enumerate(output_ensemble):
            output_ensemble[i] = torch.cat(prediction)
        output_ensemble = torch.stack(output_ensemble)

        # Calculate avg. predictions over ensemble member
        mean_predictions = torch.mean(output_ensemble, dim=0)

        return mean_predictions, output_ensemble, output_targets

    def load(self, dir_path, device=None):
        """ Loads statedicts (*.pt) into 'ens_models'

        The statedict need to match the defined neural network 'model'
        of the ensemble to be loaded successfully.

        Args:
            dir_path (string): Path to a folder of .pt state dicts building an esemble
        """
        if device is None:
            device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        # Get all state dict paths
        state_dicts_paths = glob.glob(os.path.join(dir_path, "*.pt"))

        assert (len(state_dicts_paths) > 0), "No Statedicts found in the directory"

        # Set ensemble size based on number of statedicts found in the directory
        if len(state_dicts_paths) < self.ens_size:
            self.ens_size = len(state_dicts_paths)

        # Load models and add them to the model list
        for path in state_dicts_paths:
            temp_model = copy.deepcopy(self.model)
            temp_model.load_state_dict(torch.load(path, map_location=device))
            temp_model.eval()
            temp_model.to(device)
            self.ens_models.append(temp_model)

    def save_model(self, dir_path, base_name=None):
        """ Save statedicts of the models in the ensemble.

        Args:
            dir_path (string): Path to folder where the model should be saved
        """
        # Set a base name if not given
        if base_name is None:
            base_name = "ENSEMBLE"

        ens_path = dir_path

        if not os.path.isdir(ens_path):
            os.makedirs(ens_path)
            print("Created directory {}".format(ens_path))

        file_name = base_name + "-model_{}.pt".format(i)
        file_path = os.path.join(ens_path, file_name)
        torch.save(model.state_dict(), file_path)
        print("State dict of model {} saved ({})".format(i, file_path))

    def _fgsm(self, data, data_grad):
        """ Perturbes a picture with the "fast gradient sign" method.

        Add noise to an image based on its gradient. Epsilon controls the amount
        of perbutation. Perbutated image is calculated by: X + eps * sign(∇(J(X, y))).
        Method was presented in: https://arxiv.org/pdf/1611.01236.pdf

        Args:
            image (torch.Tensor): Normalized image to be pertubated
            data_grad (torch.Tensor): Gradient of the image
            epsilon (int): Factor

        Returns:
            torch.Tensor: pertubated image(s)
        """
        # FGSM
        perturbed_image = data + self.adverserial_epsi * data_grad.sign()
        # Remove incorrect values
        return torch.clamp(perturbed_image, 0, 1)


class ModelWithTemperature(nn.Module):
    '''
    implementation for paper On Calibration of Modern neural Networks https://arxiv.org/pdf/1706.04599.pdf
    based on: https://github.com/gpleiss/temperature_scaling

    decorator that wraps a model with temperature scaling

    Args:
        model(nn.Module): a classification NN
            output should be classification logits not softmax/log softmax
    '''

    def __init__(self, model, device=None):
        super(ModelWithTemperature, self).__init__()
        self.model = model
        self.temperature = nn.Parameter(torch.ones(1) * 1.5)  # TODO: why 1.5?

        # Use CUDA if available
        if device is None:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        else:
            self.device = device

    def forward(self, input):
        logits = self.model(input)
        return self.temperature_scale(logits)

    def temperature_scale(self, logits):
        '''
        Performs temperature scaling on logits
        '''
        temperature = self.temperature.unsqueeze(1).expand(logits.size(0), logits.size(1))
        return logits / temperature

    def set_temperature(self, valid_loader):
        '''
        Learn temperature parameter T using the validation set.
        T is chosen to minimize/optimize NLL.

        Args:
            valid_loader: DataLoader of validation set (with saved indices from training the DeseNet)
        '''

        self.to(self.device)
        # criterion based on which T is to be learned
        nll_criterion = nn.CrossEntropyLoss().to(self.device)

        # logits (not softmax score)
        logits_list = []
        labels_list = []

        with torch.no_grad():
            for input, label in valid_loader:
                input = input.to(self.device)
                logits = self.model(input)
                logits_list.append(logits)
                labels_list.append(label)

            logits = torch.cat(logits_list).to(self.device)
            labels = torch.cat(labels_list).to(self.device)

        # ECE and NLL before Temperature Scaling
        before_ts_nll = nll_criterion(logits, labels).item()
        softmaxes = F.softmax(logits, dim=1)
        logits_cpu = softmaxes.cpu()
        labels_cpu = labels.cpu()
        before_ts_ece, _ = ece_mce_calc(logits_cpu, labels_cpu)

        # reliability_diagram(logits_cpu, labels_cpu)
        print('Before temperature - NLL: %.3f, ECE: %.3f' % (before_ts_nll, before_ts_ece.item()))

        # optimize T w.r.t. NLL
        optimizer = optim.LBFGS([self.temperature], lr=0.01, max_iter=50)

        def eval():
            loss = nll_criterion(self.temperature_scale(logits), labels)
            loss.backward()
            return loss
        optimizer.step(eval)

        # ECE and NLL after TS
        ts_logits_cpu = F.softmax(self.temperature_scale(logits), dim=1).cpu()

        after_ts_nll = nll_criterion(self.temperature_scale(logits), labels).item()
        after_ts_ece, _ = ece_mce_calc(ts_logits_cpu, labels_cpu)
        # reliability_diagram(ts_logits_cpu, labels_cpu)

        print('Optimal temperature: %.3f' % self.temperature.item())
        print('After temperature - NLL: %.3f, ECE: %.3f' % (after_ts_nll, after_ts_ece.item()))

        return self
