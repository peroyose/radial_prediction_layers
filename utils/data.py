import os
from urllib.request import urlretrieve

import numpy as np
import scipy.io
import matplotlib.pyplot as plt

import torch
import torchvision.datasets as datasets
import torchvision.transforms as transforms
import torchvision.utils as vutils
from torch.utils.data.sampler import SubsetRandomSampler


def get_MNIST_loader(path, train=True, batch_size=0, shuffle=True, workers=1, verbose=False):
    """ Creates a PyTorch dataloader for the MNIST dataset.

    """

    # Preprocessing
    transformer = transforms.Compose([transforms.ToTensor()])

    # (Down)Load MNIST
    mnist_set = datasets.MNIST(root=path, train=train, download=True, transform=transformer)

    # If batch_size is 0 set full batch
    if batch_size == 0:
        batch_size = mnist_set.data.shape[0]

    dataloader = torch.utils.data.DataLoader(mnist_set, batch_size=batch_size, shuffle=shuffle, num_workers=workers)

    if verbose:
        first_batch = next(iter(dataloader))
        plt.figure(figsize=(8, 8))
        plt.axis("off")
        plt.title("MNIST Examples")
        plt.imshow(np.transpose(vutils.make_grid(first_batch[0][:batch_size], padding=2, normalize=True).cpu(), (1, 2, 0)))

    return dataloader


def get_NotMNIST_loader(path, batch_size=18724, shuffle=True, workers=1, verbose=False):
    """ Creates a PyTorch dataloader for the notMNIST dataset.

    """

    dir_path = os.path.join(path, "notMNIST")

    # Check if the notMnist folder already exist
    if not os.path.isdir(dir_path):
        os.makedirs(dir_path)

    file_path = os.path.join(dir_path, "notMNIST_small.mat")

    # Check if notMNIST is already downloaded
    if not os.path.isfile(file_path):
        print("Start downloading notMNIST dataset.")
        urlretrieve("http://yaroslavvb.com/upload/notMNIST/notMNIST_small.mat", file_path)
        # wget.download("http://yaroslavvb.com/upload/notMNIST/notMNIST_small.mat", out=dir_path)
        print("Finished download.")

    notmnist = scipy.io.loadmat(file_path)
    notmnist_x = np.array(notmnist["images"]).reshape(28 * 28, 18724).transpose() / 255
    notmnist_y = np.array(notmnist["labels"]).astype(float)

    # Convert numpy arrays to pytorch tensors
    notmnist_images = torch.from_numpy(notmnist_x).view(18724, 1, 28, 28)
    notmnist_images = notmnist_images.type(torch.float32)
    notmnist_targets = torch.from_numpy(notmnist_y)

    # Create dataset and dataloader
    dataset = torch.utils.data.TensorDataset(notmnist_images, notmnist_targets)

    dataloader = torch.utils.data.DataLoader(dataset,
                                             batch_size=batch_size,
                                             shuffle=shuffle,
                                             num_workers=workers)

    if verbose:
        first_batch = next(iter(dataloader))
        plt.figure(figsize=(8, 8))
        plt.axis("off")
        plt.title("notMNIST Examples")
        plt.imshow(np.transpose(vutils.make_grid(first_batch[0][:batch_size], padding=2, normalize=True).cpu(), (1, 2, 0)))

    return dataloader


def get_CIFAR10_loader(path, batch_size=0, train=True, shuffle=True, workers=1):
    transform = transforms.Compose([transforms.Resize(256),
                                    transforms.RandomCrop(224),
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                                    ])

    # (Down)Load MNIST
    cifar10_set = datasets.CIFAR10(root=path, train=train, download=True, transform=transform)

    # If batch_size is 0 set full batch
    if batch_size == 0:
        batch_size = cifar10_set.data.shape[0]

    dataloader = torch.utils.data.DataLoader(cifar10_set, batch_size=batch_size, shuffle=shuffle, num_workers=workers)

    return dataloader


def get_CIFAR100_loader(path, batch_size=0, train=True, shuffle=True, workers=1):
    transform = transforms.Compose([transforms.Resize(256),
                                    transforms.RandomCrop(224),
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.5071, 0.4867, 0.4408), (0.2675, 0.2565, 0.2761))
                                    ])

    # (Down)Load MNIST
    cifar100_set = datasets.CIFAR100(root=path, train=train, download=True, transform=transform)

    # If batch_size is 0 set full batch
    if batch_size == 0:
        batch_size = cifar100_set.data.shape[0]

    dataloader = torch.utils.data.DataLoader(cifar100_set, batch_size=batch_size, shuffle=shuffle, num_workers=workers)

    return dataloader


def get_SVHN_loader(path, batch_size=0, split="test", shuffle=True, workers=1):
    transform = transforms.Compose([transforms.Resize(256),
                                    transforms.RandomCrop(224),
                                    transforms.ToTensor(),
                                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
                                    ])

    # (Down)Load MNIST
    svhn_set = datasets.SVHN(root=path, split=split, download=True, transform=transform)

    # If batch_size is 0 set full batch
    if batch_size == 0:
        batch_size = svhn_set.data.shape[0]

    dataloader = torch.utils.data.DataLoader(svhn_set, batch_size=batch_size, shuffle=shuffle, num_workers=workers)

    return dataloader


def get_ImageNet_dataloader(path, batch_size=0, image_size=224, shuffle=True, workers=1, verbose=False):
    """

    image_size for inception 299 else 224 for pretrained_models
    """
    data_transforms = {'train': transforms.Compose([transforms.RandomResizedCrop(image_size),
                                                    transforms.RandomHorizontalFlip(),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]),
                       'val': transforms.Compose([transforms.Resize(image_size),
                                                  transforms.CenterCrop(image_size),
                                                  transforms.ToTensor(),
                                                  transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])]),
                       'novel': transforms.Compose([transforms.Resize(image_size),
                                                    transforms.CenterCrop(image_size),
                                                    transforms.ToTensor(),
                                                    transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
                       }

    # Crawl the given folder for 'train' and 'val' data and apply transformation
    image_datasets = {x: datasets.ImageFolder(os.path.join(path, x),
                                              data_transforms[x]) for x in ['train', 'val', 'novel']}

    # Create training and validation dataloaders
    dataloader_dict = {x: torch.utils.data.DataLoader(image_datasets[x],
                                                      batch_size=batch_size,
                                                      shuffle=shuffle,
                                                      num_workers=workers) for x in ['train', 'val', 'novel']}
    # Todo, rewrite like MNIST plot function
    if verbose:
        images, classes = next(iter(dataloader_dict['train']))
        batch = vutils.make_grid(images, padding=5)
        batch = batch.numpy().transpose((1, 2, 0))
        batch = np.array([0.485, 0.456, 0.406]) * batch + np.array([0.229, 0.224, 0.225])
        batch = np.clip(batch, 0, 1)
        plt.figure(figsize=(64, 64))
        plt.axis('off')
        plt.imshow(batch)

    return dataloader_dict


class CIFAR100DataLoaderBuilder():
    def __init__(self, path, batch_size, mean=[0.5071, 0.4867, 0.4408], std=[0.2675, 0.2565, 0.2761]):
        self.path = path
        self.mean = mean
        self.std = std
        self.batch_size = batch_size
        self._validationSize = 0

        self._trainSet = None
        self._trainIndices = None
        self._validationIndices = None

    def validate(self, validationSize):
        self._validationSize = validationSize
        return self

    def _getTrainSet(self):
        if self._trainSet is None:
            transform = transforms.Compose([transforms.Resize(256),
                                            transforms.RandomCrop(224),
                                            transforms.RandomHorizontalFlip(),
                                            transforms.ToTensor(),
                                            transforms.Normalize(mean=self.mean, std=self.std)])
            self._trainSet = datasets.CIFAR100(root=self.path, train=True, transform=transform, download=True)
        return self._trainSet

    def buildIndices(self):
        if self._trainIndices is None:
            indices = torch.randperm(len(self._getTrainSet()))
            self._trainIndices = indices[:len(indices) - self._validationSize]
            if self._validationSize > 0:
                self._validationIndices = indices[len(indices) - self._validationSize:]

        return self._trainIndices, self._validationIndices

    def buildTrainLoader(self, workers=1):
        train_set = self._getTrainSet()

        if self._validationSize > 0:
            train_indices, _ = self.buildIndices()
            return torch.utils.data.DataLoader(train_set,
                                               pin_memory=True,
                                               batch_size=self.batch_size,
                                               sampler=SubsetRandomSampler(train_indices))
        else:
            return torch.utils.data.DataLoader(train_set, batch_size=self.batch_size, shuffle=True, num_workers=workers)

    def buildValidLoader(self, indices=None):
        transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize(mean=self.mean, std=self.std)])
        valid_set = datasets.CIFAR100(root=self.path, train=True, transform=transform, download=False)

        if self._validationSize > 0 and indices is None:
            _, indices = self.buildIndices()

        return torch.utils.data.DataLoader(valid_set, pin_memory=True, batch_size=self.batch_size, sampler=SubsetRandomSampler(indices))

    def buildTestLoader(self, workers=1):
        transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize(mean=self.mean, std=self.std)])
        test_set = datasets.CIFAR100(root=self.path, train=False, transform=transform, download=True)
        return torch.utils.data.DataLoader(test_set, batch_size=self.batch_size, shuffle=False, num_workers=4)
