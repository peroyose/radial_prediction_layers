# Radial Prediction Layer

This repository reproduces the results of [Radial Prediction Layer](https://imgs.xkcd.com/comics/research_risks.png). RPL is a classification layer for artificial neural networks, which can be used instead of Softmax. In contast to Softmax, RPL is able to model uncertainty.

<p align="center" float="left">
  <img src="https://redmine.f4.htw-berlin.de/owncloud/index.php/s/4sDCJyKkqzmNLpT/preview" width="49%">
  <img src="https://redmine.f4.htw-berlin.de/owncloud/index.php/s/y3y3JbzHzftccgs/preview" width="49%">
</p>

## Requirements

If using conda you can create a proper vitual env using the provided `environment.yml`. As an alternative use the `requirements.txt`.

```bash
# using conda
conda env create -f environment.yml
conda activate rpl
```

```bash
# using pip
pip install -r requirements.txt

# using conda
conda create --name rpl --file requirements.txt
conda activate rpl
```

## Evaluation and Result Reproduction using Pre-trained Models

Data set specific Jupyter notebooks are available to reproduce the results from the paper. For the spiral dataset no additional resources are required. Simple run the whole notebook and the model will be trained and evaluated. We provide pre-trained models for all other datasets how are more time-consuming in their trainings process. You can download our trained models using the script `get_pretrtained.py` or directly during the notebook execution. If you choose to download the corresponding model in the notebook, please change the corresponding cell from typ `raw` to `code` before running the notebook.

**Pre-trained Models**

In order to enable correct reproduction, the models used in the paper have to be downloaded. The script `get_pretrtained.py` can be executed to download all models or only these for a specific data set, use the appropriate argument. Model archives vary in file size, which should be considered before downloading all models at once.

- MNIST: 2.3 MB
- CIFAR10: 12.3 GB
- CIFAR100: 12.4 GB
- IMAGENET: 8.2 GB

```bash
python get_pretrtained.py --data [ALL|MNIST|CIFAR10|CIFAR100|IMAGENET]
```

## Training 

### MNIST and CIFAR

To train models used in the experiments by your own execute the `train.py` on a specific data set. Experimental details and hyperparameters are configured via data set specific `.yaml` files in the `./config` directory. The current settings of each configuration file corresponds to the hyperparameter settings used in the paper. Used data sets are downloaded automatictly during the training process.

```bash
python train.py --data [MNIST|CIFAR10|CIFAR100]
```

### IMAGENET

An exception is the ILSVRC data (ImageNet) which is not public avaiable. To train models from the ImageNet experiments download the last public data set from the [project website](http://image-net.org/), unzip and preprocess it using the following steps:

Copy the compressed data file to the project data folder or any place you keep your data sets and unzip it.
```
mv ~/Downloads/ILSVRC2017_CLS-LOC.tar.gz ~/$RPL_project/data/imagenet/ILSVRC2017_CLS-LOC.tar.gz
tar -xzvf ILSVRC2017_CLS-LOC.tar.gz
```

To divide the original data into train, validation and novel data points used in the paper, execute the provided scripts inside the imagenet `validation data` directory. The script renames and reorder sthe folder structure to work with `torchvision.datasets.ImageFolder()` and ensures the data is loaded as three different sets.

```bash
# Execute inside the val data folder, e.g.
# cd $MyImageNetFolder/ILSVRC/Data/CLS-LOC/val
wget -qO- https://raw.githubusercontent.com/soumith/imagenetloader.torch/master/valprep.sh | bash 
wget -qO- https://gitlab.com/peroyose/radial_prediction_layers/blob/master/data/imagenet/create_subart.sh | bash

```

Know edit the `data_path` setting in the `imagenet.yaml` config file to point to the correct location. If the data set is inside the correct project directory or linked to it, you may skip that step.  

```bash
...
data_path: $My_path_to_ImageNet_SubArt_data
...
```

Now, you should be able to train the models for the ImageNet data as well.

```bash
python train.py --data IMAGENET
```
