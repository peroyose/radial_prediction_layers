import torch
import torch.nn as nn


class RadialPredictionLayer(torch.nn.Module):
    """ Radial Prediction Layer with fixed or learned prototypes

    Params:
    -------
    in_features: Int
        Input dimension of the layer
    out_features: Int - default: None
        Output dimesion is by default set to the input dim, but can be set
    prototype_fixed: Bool - default: False
        Initialize prototypes on axis or learn the positions
    """

    def __init__(self, in_features, out_features=None, prototype_fixed=False, alpha=1.0):
        super(RadialPredictionLayer, self).__init__()

        self.in_features = in_features
        self.prototype_fixed = prototype_fixed
        self.alpha = alpha

        if out_features is None:
            self.out_features = in_features
        else:
            self.out_features = out_features

        if self.prototype_fixed:
            assert self.in_features == self.out_features , "'if prototypes are fixed 'in_features' and 'out_features' need to be equal"
            # Initialize prototyp unit vectors on each axis
            self.prototypes = nn.Parameter(torch.diag((torch.ones(self.in_features) * self.alpha)), requires_grad=False)
        else:
            self.prototypes = nn.Parameter(self.init_prototypes(self.in_features, self.out_features))

    def forward(self, x):
        """ Forward path calculates the squared euclidean between each point and each prototype
        """
        return (-(((x[:, None, :] - self.prototypes[None, :, :])**2).sum(dim=2))) # None is alias for np.newaxis, dim2 = z axis
        #return (-(((x[:, None, :] - self.prototypes[None, :, :])**2).sum(dim=2)).sqrt())

    def extra_repr(self):
        """ Create an output string with Layer parameters
        """
        return 'in_features={}, out_features={}, prototypes_fixed={}'.format(
            self.in_features, self.out_features, self.prototype_fixed
        )

    def init_prototypes(self, prototyp_dim, num_classes):
        """ Inialize prototype vectors with numbers drawn from a normal distribution
            with mean = 0 and std = 1.
        """
        params = [num_classes, prototyp_dim]
        prototyp_vecs = torch.normal(torch.zeros(params), torch.ones(params))
        return prototyp_vecs


class RadialPredictionLoss(torch.nn.Module):
    """ Custom loss to train networks with the radial prediction layer

    Params:
    -------
    device: torch.device - default: CPU
        Define to use GPU or CPU
    """

    def __init__(self, prototype_fixed=False, device=torch.device("cpu")):
        super(RadialPredictionLoss, self).__init__()
        self.device = device
        self.prototype_fixed = prototype_fixed

    def forward(self, predictions, targets):
        """ Calculate a loss for a network using an RPL

        Params:
        -------
        predictions:
            Predictions of an RPL
        targets:
            Ground truth
        prototype_fixed: Bool - default: False
            Define if the RPL was initialized with fixed prototypes
        """

        batch_size = predictions.shape[0]
        class_num = predictions.shape[1]

        if self.prototype_fixed:
            # Crossentropy average as pytorch tensor (scalar)
            # Fancy indexing, replaces 1-hot-enc of the targets
            loss = - predictions[range(batch_size), targets]
        else:
            # Create one-hot-enc targets
            targets_one_hot = torch.eye(class_num)[targets].to(self.device) # eye returns 2d tensor with ones on the diagonal 
            # Custom loss function - up to research
            loss = - targets_one_hot * predictions + (1 - targets_one_hot) * predictions.exp()
        return loss.sum() / batch_size
