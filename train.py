#!/usr/bin/env python
# coding: utf-8
import os
import time
import logging
import copy
import numpy as np

import argparse
import confuse

import torch

from rpl.rpl_pt import RadialPredictionLoss
from utils.networks import LeNet, AlexNet, VGG19, ResNet50, InceptionV3
from utils.data import get_MNIST_loader, get_CIFAR10_loader, get_ImageNet_dataloader, CIFAR100DataLoaderBuilder
from utils.uncertainty import DeepEnsemble, ModelWithTemperature
from utils.utils import AverageMeter, accuracy, print_learnable_params


# Initialize argparser, confuse configuration and logger
logging.basicConfig(filename='train.log', format='%(asctime)s %(message)s', level=logging.INFO)
config = confuse.Configuration('RPL')
parser = argparse.ArgumentParser("RPL Experiments Train-Parser")

# Argparse arguments
parser.add_argument('--data', type=str, choices=["MNIST", "CIFAR10", "CIFAR100", "IMAGENET"], help="Run experiments for a specific data set.")
parser.add_argument("--seed", default=20, type=int, help="Random seed for numpy and PyTorch (default: 20)")
parser.add_argument("--gpu", default=0, type=int, help="GPU device")

# Initialize Parser and set general settings - GPU usage and random seeds
args = parser.parse_args()

# Set seeds
torch.manual_seed(args.seed)

# Set device
device = torch.device(args.gpu if torch.cuda.is_available() else "cpu")


def _get_appropriate_model(method, network):
    """ Selects and initializes a network architecture using config information.

    TODO: Replace by dict or something more clean
    """
    if method == "SOFT" or method == "SOFT_ENSEMBLE":
        if network == "LeNet":
            return LeNet(dropout=config["experiments"][method][network]["dropout"].get(float))
        if network == "AlexNet":
            return AlexNet(num_classes=config["num_classes"].get(int),
                           dropout=config["experiments"][method][network]["dropout"].get(float))
        if network == "VGG":
            return VGG19(num_classes=config["num_classes"].get(int),
                         pretrained=config["experiments"][method][network]["pretrained"].get(bool),
                         freeze_pretrained=config["experiments"][method][network]["freeze_pretrained"].get(bool),
                         dropout=config["experiments"][method][network]["dropout"].get(float))
        if network == "ResNet":
            return ResNet50(num_classes=config["num_classes"].get(int),
                            pretrained=config["experiments"][method][network]["pretrained"].get(bool),
                            freeze_pretrained=config["experiments"][method][network]["freeze_pretrained"].get(bool),
                            dropout=config["experiments"][method][network]["dropout"].get(float))
        if network == "Inception":
            return InceptionV3(num_classes=config["num_classes"].get(int),
                               pretrained=config["experiments"][method][network]["pretrained"].get(bool),
                               freeze_pretrained=config["experiments"][method][network]["freeze_pretrained"].get(bool),
                               dropout=config["experiments"][method][network]["dropout"].get(float))
    if method == "RPL" or method == "RPL_ENSEMBLE":
        if network == "LeNet":
            return LeNet(use_rpl=True,
                         dropout=config["experiments"][method][network]["dropout"].get(),
                         alpha=config["experiments"][method][network]["alpha"].get())
        if network == "AlexNet":
            return AlexNet(num_classes=config["num_classes"].get(int),
                           use_rpl=True,
                           dropout=config["experiments"][method][network]["dropout"].get(),
                           alpha=config["experiments"][method][network]["alpha"].get())
        if network == "VGG":
            return VGG19(num_classes=config["num_classes"].get(int),
                         pretrained=config["experiments"][method][network]["pretrained"].get(bool),
                         freeze_pretrained=config["experiments"][method][network]["freeze_pretrained"].get(bool),
                         use_rpl=True,
                         dropout=config["experiments"][method][network]["dropout"].get(float),
                         alpha=config["experiments"][method][network]["alpha"].get(float))
        if network == "ResNet":
            return ResNet50(num_classes=config["num_classes"].get(int),
                            pretrained=config["experiments"][method][network]["pretrained"].get(bool),
                            freeze_pretrained=config["experiments"][method][network]["freeze_pretrained"].get(bool),
                            use_rpl=True,
                            dropout=config["experiments"][method][network]["dropout"].get(float),
                            alpha=config["experiments"][method][network]["alpha"].get(float))
        if network == "Inception":
            return InceptionV3(num_classes=config["num_classes"].get(int),
                               pretrained=config["experiments"][method][network]["pretrained"].get(bool),
                               freeze_pretrained=config["experiments"][method][network]["freeze_pretrained"].get(bool),
                               use_rpl=True,
                               dropout=config["experiments"][method][network]["dropout"].get(float),
                               alpha=config["experiments"][method][network]["alpha"].get(float))


def _get_appropriate_loss(method):
    """
    """
    if method == "RPL":
        return RadialPredictionLoss(prototype_fixed=True)
    else:
        return torch.nn.functional.nll_loss


# Vanilla train method
def _train_default(model, data, optimizer, criterion, epochs=10):
    model.train()
    model.to(device)

    since = time.time()
    logging.info("Start training...")
    for e in range(epochs):
        # Initialize log vars
        loss_sum = 0
        correct_preds = 0
        accuracy = 0

        for mini_batch in data:
            # Move batch to device
            x = mini_batch[0].to(device)
            y = mini_batch[1].to(device)

            # Clean buffer
            optimizer.zero_grad()

            # Calculate output and loss
            output = model.forward(x)
            loss = criterion(output, y)

            # Backprop and optimze
            loss.backward()
            optimizer.step()

            # Accumulate batch loss and number of correct classified samples
            loss_sum += loss
            correct_preds += (y == output.argmax(dim=1)).sum()

        epoch_loss = loss_sum / len(data)
        accuracy = correct_preds.item() / len(data.dataset)
        logging.info("E[{}] || Loss: {:4f} || Correct Classified: {} || Accuracy: {:4f}".format(e, epoch_loss, correct_preds, accuracy))

    # Console hearthbeat and logging after training
    time_elapsed = time.time() - since
    print("Model training finished after {:.0f}m {:.0f}s with following stats (Loss: {:4f} || Accuracy: {:4f})".format(time_elapsed // 60, time_elapsed % 60, epoch_loss, accuracy))
    logging.info("Training finished after {:.0f}m {:.0f}s\n".format(time_elapsed // 60, time_elapsed % 60))
    return model, accuracy


def _exp_pipeline_default(data):
    """
    """
    for method in config["experiments"]:
        logging.info("Uncertainty method: {}".format(method))
        print("Training model for uncertainty method: {}".format(method))

        for network in config["experiments"][method]:
            # Fetch the corresponding model for the experiment
            model = _get_appropriate_model(method, network)
            logging.info("Training - Method: {} || Model: {}".format(method, model))
            print_learnable_params(model)

            # Distribute over GPUs if available
            if torch.cuda.device_count() > 1:
                logging.info("Model capsuled in DataParallel object to train on multiple GPUs")
                model = torch.nn.DataParallel(model)

            if "ENSEMBLE" in method:
                ensemble = DeepEnsemble(model, ensemble_size=config["experiments"][method][network]["ens_size"].get(int), device=device)
                optimizer = torch.optim.Adam
                loss = _get_appropriate_loss(method)
                file_path = os.path.join(config["model_path"].get(str), method + "-" + network)
                ensemble.train(data,
                               optimizer,
                               loss,
                               epochs=config["epochs"].get(int),
                               lr=config["experiments"][method][network]["learning_rate"].get(float),
                               adverserial_epsi=config["experiments"][method][network]["epsilon"].get(float),
                               ensemble_path=file_path,
                               verbose=True)

                # Clean up ensemble
                del ensemble
            else:
                # Initialize optimizer with specific configuration
                optimizer = torch.optim.Adam(model.parameters(),
                                             lr=config["experiments"][method][network]["learning_rate"].get(float))

                # Initialize loss bases on the method used
                loss = _get_appropriate_loss(method)

                # Train the model
                model, acc = _train_default(model, data, optimizer, loss, epochs=config["epochs"].get(int))

                # Create file name "$method-$network-$acc-$model_name_ext.pt" and save state_dict
                try:
                    name_ext = "-" + config["experiments"][method][network]["model_name_ext"].get(str)
                except confuse.NotFoundError:
                    name_ext = ""
                file_name = method + "-" + network + "-" + "acc{:.4f}".format(acc).replace(".", "p") + name_ext + ".pt"
                file_path = os.path.join(config["model_path"].get(str), file_name)

                # If we calculated on multiple GPUs save model without DataParallel
                if isinstance(model, torch.nn.DataParallel):
                    torch.save(model.module.state_dict(), file_path)
                else:
                    torch.save(model.state_dict(), file_path)

                logging.info("State dict saved ({})".format(file_path))

                if method == "SOFT":
                    try:
                        if config["experiments"]["SOFT"][network]["temp_scale"].get(bool):
                            temp_scale_model = ModelWithTemperature(model)
                            temp_scale_model.set_temperature(data)
                            temp_file_path = file_path.split(".pt")
                            temp_file_path = temp_file_path[0] + "-temp_scaled.pt"
                            if isinstance(model, torch.nn.DataParallel):
                                temp_scale_model.model = temp_scale_model.model.module
                            torch.save(temp_scale_model.state_dict(), temp_file_path)
                            del temp_scale_model
                    except confuse.NotFoundError:
                        pass

            # Clean up
            del model, optimizer, loss
            torch.cuda.empty_cache()


def _train_imagenet(model, dataloader_dict, criterion, optimizer, epochs=13, verbose=False):
    """ Main training loop

    Args:
        model: Radial Decision Network
        dataloaders: Data using torch.utils.data.DataLoader
        criterion: Loss metric
        optimizer: Optimizer using torch.optim
        num_epochs: Number of training epochs
        is_inception: Use loop for 'inception' architectures

    Returns:
        model:  Model with the best accuracy
        val_acc_history: Validation accuracy history
    """
    best_model_wts = copy.deepcopy(model.state_dict())

    best_acc1 = 0.0
    best_acc5 = 0.0

    train_losses = AverageMeter('Loss', ':.4e')
    train_top1 = AverageMeter('Acc1', ':5.2f')
    train_top5 = AverageMeter('Acc5', ':5.2f')
    train_epoch_top1 = AverageMeter('EpAcc1', ':5.2f')
    train_epoch_top5 = AverageMeter('EpAcc5', ':5.2f')

    val_losses = AverageMeter('Loss', ':.4e')
    val_top1 = AverageMeter('Acc1', ':5.2f')
    val_top5 = AverageMeter('Acc5', ':5.2f')

    logging.info("Start training...")
    train_start_time = time.time()

    model = model.to(device)

    for epoch in range(epochs):
        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            # Iterate over data.
            for step, (inputs, labels) in enumerate(dataloader_dict[phase]):
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    outputs = model(inputs)
                    loss = criterion(outputs, labels)

                    # backward + feature optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # Log Batch Statistics
                acc1, acc5 = accuracy(outputs, labels, topk=(1, 5))
                if phase == 'train':
                    train_top1.update(acc1.item(), inputs.size(0))
                    train_top5.update(acc5.item(), inputs.size(0))
                    train_losses.update(loss.item())
                else:
                    val_top1.update(acc1.item(), inputs.size(0))
                    val_top5.update(acc5.item(), inputs.size(0))
                    val_losses.update(loss.item())

                # Batch logging
                if verbose is True and phase == 'train':
                    if step % 100 == 0:
                        logging.info("[{}/{}] || {} || {} || {}".format(step,
                                                                        len(dataloader_dict[phase]),
                                                                        train_losses,
                                                                        train_top1,
                                                                        train_top5))
            # Epoch logging
            if phase == 'train':
                train_epoch_top1.update(acc1.item())
                train_epoch_top5.update(acc5.item())
                logging.info("[T] || E: [{}/{}] || {} || {} || {} ".format(epoch + 1, epochs, train_losses, train_top1, train_top5))
            else:
                logging.info("[V] || E: [{}/{}] || {} || {} || {} ".format(epoch + 1, epochs, val_losses, val_top1, val_top5))

            # deep copy the model
            if phase == 'val' and val_top1.val >= best_acc1:
                best_acc1 = val_top1.val
                best_acc5 = val_top5.val
                best_model_wts = copy.deepcopy(model.state_dict())

    time_elapsed = time.time() - train_start_time
    logging.info("...training completed in {:.0f}m {:.0f}s".format(time_elapsed // 60, time_elapsed % 60))
    logging.info("Best Validtion Acc: {:.4f}, {:.4f}".format(best_acc1, best_acc5))

    # load best model weights
    model.load_state_dict(best_model_wts)
    train_stats = [train_losses, train_top1, train_top5, train_epoch_top1, train_epoch_top5]
    val_stats = [val_losses, val_top1, val_top5]

    return model, train_stats, val_stats, best_acc1


def _exp_pipeline_imagenet(data, log_train_stats=False):
    """
    """
    for method in config["experiments"]:
        logging.info("Uncertainty method: {}".format(method))
        print("Training model for uncertainty method: {}".format(method))

        for network in config["experiments"][method]:
            # Fetch the corresponding model for the experiment
            model = _get_appropriate_model(method, network)
            logging.info("Model: %s", model)

            # Distribute over GPUs if available
            if torch.cuda.device_count() > 1:
                logging.info("Model capsuled in DataParallel object to train on multiple GPUs")
                model = torch.nn.DataParallel(model)

            if "ENSEMBLE" in method:
                ensemble = DeepEnsemble(model, ensemble_size=config["experiments"][method][network]["ens_size"].get(int), device=device)
                optimizer = torch.optim.Adam
                loss = _get_appropriate_loss(method)
                file_path = os.path.join(config["model_path"].get(str), method + "-" + network)
                ensemble.train(data['train'],
                               optimizer,
                               loss,
                               epochs=config["epochs"].get(int),
                               lr=config["experiments"][method][network]["learning_rate"].get(float),
                               adverserial_epsi=config["experiments"][method][network]["epsilon"].get(float),
                               ensemble_path=file_path,
                               verbose=True)

                # Clean up ensemble
                del ensemble

            else:
                # Initialize optimizer with specific configuration
                optimizer = torch.optim.Adam(model.parameters(),
                                             lr=config["experiments"][method][network]["learning_rate"].get(float))

                # Initialize loss bases on the method used
                loss = _get_appropriate_loss(method)

                # Train the model
                model, train_stats, val_stats, acc = _train_imagenet(model, data, loss, optimizer, config["epochs"].get(int), True)

                # Create model file "$method-$network-$acc-$model_name_ext.pt" and save state_dict
                try:
                    name_ext = "-" + config["experiments"][method][network]["model_name_ext"].get(str)
                except confuse.NotFoundError:
                    name_ext = ""
                file_name = method + "-" + network + "-" + "acc{:.4f}".format(acc).replace(".", "p") + name_ext + ".pt"
                file_path = os.path.join(config["model_path"].get(str), file_name)

                # If we calculated on multiple GPUs save model without DataParallel
                if isinstance(model, torch.nn.DataParallel):
                    torch.save(model.module.state_dict(), file_path)
                else:
                    torch.save(model.state_dict(), file_path)

                logging.info("State dict saved ({})".format(file_path))

                # Save training report
                if log_train_stats:
                    temp_stats = {}
                    for metric in train_stats:
                        temp_stats[metric.name] = metric.hist
                    np.savez(file_path.replace(".pt", "") + "-train_mectrics", **temp_stats)
                    temp_stats = {}
                    for metric in val_stats:
                        temp_stats[metric.name] = metric.hist
                    np.savez(file_path.replace(".pt", "") + "-validation_metrics", **temp_stats)

                # Do temp scaling for Softmax models
                if method == "SOFT":
                    try:
                        if config["experiments"]["SOFT"][network]["temp_scale"].get(bool):
                            temp_scale_model = ModelWithTemperature(model)
                            temp_scale_model.set_temperature(data["val"])
                            temp_file_path = file_path.split(".pt")
                            temp_file_path = temp_file_path[0] + "-temp_scaled.pt"
                            if isinstance(model, torch.nn.DataParallel):
                                temp_scale_model.model = temp_scale_model.model.module
                            torch.save(temp_scale_model.state_dict(), temp_file_path)
                            del temp_scale_model
                    except confuse.NotFoundError:
                        pass

            # Clean up
            del model, optimizer, loss, train_stats, val_stats, temp_stats
            torch.cuda.empty_cache()


# Schedule experiments for a dataset
if "MNIST" == args.data:
    print("Experiments for MNIST")
    logging.info("Beginning MNIST experiments")

    # Load MNIST configuration file and log it
    config.set_file("config/mnist.yaml")
    logging.info(config.dump())

    # Create model directory
    if not os.path.isdir(config["model_path"].get(str)):
        os.makedirs(config["model_path"].get(str))
        print("Created directory {}".format(config["model_path"].get(str)))

    # Load MNIST train data
    mnist_data = get_MNIST_loader(config["data_path"].get(str), train=True, batch_size=config["batch_size"].get(int), workers=4)

    # Runs all experiments defined in the mnist.yaml config file
    _exp_pipeline_default(mnist_data)

    logging.info("Finished MNIST experiments\n")

elif "CIFAR10" == args.data:
    print("Experiments for CIFAR10")
    logging.info("Beginning CIFAR10 experiments")

    # Load CIFAR10 configuration file and log it
    config.set_file("config/cifar10.yaml")
    logging.info(config.dump())

    # Create model directory
    if not os.path.isdir(config["model_path"].get(str)):
        os.makedirs(config["model_path"].get(str))
        print("Created directory {}".format(config["model_path"].get(str)))

    # Load MNIST data
    cifar10_data = get_CIFAR10_loader(config["data_path"].get(str), train=True, batch_size=config["batch_size"].get(), workers=4)

    # Runs all experiments defined in the mnist.yaml config file
    _exp_pipeline_default(cifar10_data)

    logging.info("Finished CIFAR10 experiments\n")

elif "CIFAR100" == args.data:
    print("Experiments for CIFAR100")
    logging.info("Beginning CIFAR100 experiments")

    # Load CIFAR10 configuration file and log it
    config.set_file("config/cifar100.yaml")
    logging.info(config.dump())

    # Create model directory
    if not os.path.isdir(config["model_path"].get(str)):
        os.makedirs(config["model_path"].get(str))
        print("Created directory {}".format(config["model_path"].get(str)))

    # Load MNIST data
    cifar100_data = CIFAR100DataLoaderBuilder(config["data_path"].get(str), batch_size=config["batch_size"].get()).buildTrainLoader()

    # Runs all experiments defined in the mnist.yaml config file
    _exp_pipeline_default(cifar100_data)

    logging.info("Finished CIFAR100 experiments\n")

elif "IMAGENET" == args.data:
    print("Experiments for ImageNet")
    logging.info("Beginning ImageNet experiments")

    # Load CIFAR10 configuration file and write into the log
    config.set_file("config/imagenet.yaml")
    logging.info(config.dump())

    # Create model directory
    if not os.path.isdir(config["model_path"].get(str)):
        os.makedirs(config["model_path"].get(str))
        print("Created directory {}".format(config["model_path"].get(str)))

    # Load ImageNet data
    imagenet_data = get_ImageNet_dataloader(config["data_path"].get(str), batch_size=config["batch_size"].get(), workers=4)

    _exp_pipeline_imagenet(imagenet_data, True)

    logging.info("Finished IMAGENET experiments\n")

else:
    print("This error message should be impossible to reach")
